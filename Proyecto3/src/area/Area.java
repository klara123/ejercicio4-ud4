package area;

import java.util.Scanner;

public class Area {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		System.out.println("Introduce el radio de un c�rculo:");
		double radio = input.nextDouble();
		
		input.close();
		
		System.out.println("�rea del c�rculo: " + (Math.pow(radio, 2) * Math.PI));

	}

}
